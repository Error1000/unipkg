#!/bin/bash

libs=""
os=""
specifiedos=false
specifiedlibs=false
tomake=""
specifiedtomake=false
specifiedmakefile=false
makefile="makefile"

while getopts "l:m:o:f:" option; do
 case "$option" in
  l) libs=$OPTARG specifiedlibs=true;;
  m) tomake=$OPTARG specifiedtomake=true;;
  o) os=$OPTARG specifiedos=true;;
  f) makefile=$OPTARG specifiedmakefile=true;;
  \?) echo error;;  
 esac
done


if [[ $specifiedtomake  = false ]]; then 
 echo "Please choose to make at least something!"
 exit 1
fi

if [[  $specifiedos = false && $specifiedlibs = true ]]; then 
 echo "Os hasn't been choosen, not installing libraries!!"
fi


if [[ $os = *arch*  || $os = arch* || $os = *arch || $os = arch ]]; then
 sudo pacman -S --noconfirm make gcc g++
 if [[ $specifiedlibs = true ]]; then
  sudo pacman -S --noconfirm  $libs
 fi

 if [[ $specifiedmakefile = false ]]; then
  makefile="makefile-linux"  
 fi

elif [[ $os = *buntu* || $os = buntu* || $os = *buntu ]]; then
 sudo apt -y install make gcc g++
 if [[ $specifiedlibs = true ]]; then
  sudo apt -y install $libs
 fi

 if [[ $specifiedmakefile = false ]]; then
  makefile="makefile-linux"  
 fi

elif [[ $os = *macos* || $os = macos* || $os = *macos || $os = *mac* || $os = mac* ]]; then
 command -v brew >/dev/null 2>&1 || ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
 #gcc using brew automatically comes with g++ 
 brew install make gcc
 if [[ $specifiedlibs = true ]]; then
  brew install $libs
 fi

 if [[ $specifiedmakefile = false ]]; then
  makefile="makefile-macos"  
 fi
fi



if [[ $specifiedmakefile = false ]]; then
 echo "Makefile name was not specified using default guessed filename: \"$makefile\"!!"
fi

if [[ $specifiedtomake = true ]]; then 
  make -f $makefile $tomake
fi


if [[ $? -ne 0 ]]; then
 echo "Return code was not zero for make!"
 exit 1
else
 echo "Done?"
 exit 0
fi

